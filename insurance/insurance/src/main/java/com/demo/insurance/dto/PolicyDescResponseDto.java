package com.demo.insurance.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PolicyDescResponseDto {
	
private String policyName;
	
	private int minEntryAge;
	private int maxEntryAge;
	private int maximumMaturityAge;
	private int minPolicyTerm;
	private int maxPolicyTerm;
	private String minimumPremium;
	private String minSumAssured;
	private String poilicyDesc;

}
