package com.demo.insurance.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.insurance.dto.PolicyTrendDto;
import com.demo.insurance.entity.PolicyTrend;
import com.demo.insurance.repository.PolicyTrendRepository;

/**
 * This class is used to find global trend policy.
 * @author sidramesh mudhol
 *
 */
@Service
public class GlobalTrendPolicyService {
	
	/**
	 * LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalTrendPolicyService.class);
	
	@Autowired
	private PolicyTrendRepository policyTrendRepository;

	/**
	 * 
	 * @return List<PolicyTrendDto>
	 */
	public List<PolicyTrendDto> getPolicyTrend() {
		LOGGER.info("inside getPolicyTrend method");
		List<PolicyTrendDto> policyTrendDtoList = new ArrayList<>();
		List<PolicyTrend> policyTrendList = policyTrendRepository.findAll();
		long count = 0;
		for (PolicyTrend policyTrend : policyTrendList) {
			count = count + policyTrend.getCount();
		}
		
		for (PolicyTrend policyTrend : policyTrendList) {
			PolicyTrendDto policyTrendDto = new PolicyTrendDto();
			int percentage=0;
			if(count!=0)
			 percentage = (int) ((policyTrend.getCount() * 100)/count);
			policyTrendDto.setPolicyId(policyTrend.getPolicyId());
			policyTrendDto.setCount(policyTrend.getCount());
			policyTrendDto.setPercentage(percentage);
			policyTrendDtoList.add(policyTrendDto);
			
		}
		LOGGER.info("exiting getPolicyTrend method");
		return policyTrendDtoList;
	}

}
