package com.demo.insurance.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demo.insurance.dto.PolicyResponseDto;
import com.demo.insurance.entity.Policy;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.repository.PolicyRepository;

/**
 * This service class is used to get all policies.
 * @author sidramesh mudhol
 *
 */
@Service
public class LICPolicyService {
	
	
	/**
	 * LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalTrendPolicyService.class);
	
	@Autowired
	PolicyRepository policyRepository;

	/**
	 * 
	 * @return List<PolicyResponseDto>
	 */
	public List<PolicyResponseDto> getAllPolicies() {
		LOGGER.info("inside getAllPolicies method");
		List<PolicyResponseDto> policyResponseDtoList = new ArrayList<>();
		List<Policy> listOfPolicies = policyRepository.findAll();
		if (!listOfPolicies.isEmpty()) {
			for (Policy policy : listOfPolicies) {
				PolicyResponseDto policyResponseDto = new PolicyResponseDto();
				BeanUtils.copyProperties(policy, policyResponseDto);
				policyResponseDtoList.add(policyResponseDto);
			}
		}
		LOGGER.info("exiting getAllPolicies method");
		return policyResponseDtoList;
	}

	/**
	 * 
	 * @param policyId
	 * @return
	 * @throws PolicyNotFoundException
	 */
	public PolicyResponseDto getPolicyById(Long policyId) throws PolicyNotFoundException {
		PolicyResponseDto policyResponseDto = new PolicyResponseDto();
		Policy policy = null;
		Optional<Policy> policyResponseOptionalDto = policyRepository.findById(policyId);
		if (policyResponseOptionalDto.isPresent()) {
			policy = policyResponseOptionalDto.get();
		} else {
			throw new PolicyNotFoundException("policy not found");
		}
		BeanUtils.copyProperties(policy, policyResponseDto);
		return policyResponseDto;
	}
	
	
	
	
	

}
