package com.demo.insurance.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table(name="policy")
public class Policy {
	
	@Id
	private long policyId;
	
	private String policyName;
	
	private int minEntryAge;
	private int maxEntryAge;
	private int maximumMaturityAge;
	private int minPolicyTerm;
	private int maxPolicyTerm;
	private String minimumPremium;
	private String minSumAssured;
	private String termCondition;
	private String poilicyDesc;
	
	
	
	
	

}
