package com.demo.insurance.service;

import java.time.LocalDate;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.insurance.constants.AppConstants;
import com.demo.insurance.controller.LICPoliciesController;
import com.demo.insurance.dto.PolicyRequestDto;
import com.demo.insurance.entity.Employee;
import com.demo.insurance.entity.EmployeePolicy;
import com.demo.insurance.entity.Policy;
import com.demo.insurance.entity.PolicyTrend;
import com.demo.insurance.exception.EmployeeNotFoundException;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.repository.EmpPolicyRepository;
import com.demo.insurance.repository.EmpRepository;
import com.demo.insurance.repository.PolicyRepository;
import com.demo.insurance.repository.PolicyTrendRepository;

/**
 * 
 * @author sidramesh mudhol
 *
 */
@Service
public class EmpPolicyService {
	
	
	/**
	 * LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpPolicyService.class);
	
	@Autowired
	private EmpRepository empRepository;
	
	@Autowired
	private PolicyRepository policyRepository;
	
	@Autowired
	private EmpPolicyRepository empPolicyRepository;
	
	@Autowired
	private PolicyTrendRepository policyTrendRepository;

	/**
	 * 
	 * @param policyRequestDto
	 * @return
	 * @throws EmployeeNotFoundException
	 * @throws PolicyNotFoundException
	 */
	public String selectPolicy(PolicyRequestDto policyRequestDto) throws EmployeeNotFoundException, PolicyNotFoundException {
		LOGGER.info("inside getAllPolicies method");
		EmployeePolicy empPolicy = new EmployeePolicy();
		Optional<Employee> optionalEmployee = empRepository.findById(policyRequestDto.getEmpId());
		if (!optionalEmployee.isPresent()) {
			LOGGER.info("exception in  getAllPolicies method");
			throw new EmployeeNotFoundException("employee not found");
		}
		
		Optional<Policy> optionalPolicy = policyRepository.findById(policyRequestDto.getPolicyId());
		if (!optionalPolicy.isPresent()) {
			LOGGER.error("exception in getAllPolicies method");
			throw new PolicyNotFoundException("policy not found");
		}
		empPolicy.setDate(LocalDate.now());
		empPolicy.setMinimumPremium(500);
		empPolicy.setEmpId(optionalEmployee.get());
		empPolicy.setPolicyId(optionalPolicy.get());
		empPolicyRepository.save(empPolicy);
		Optional<PolicyTrend> optionalPolicyTrend = policyTrendRepository.findById(optionalPolicy.get().getPolicyId());
		PolicyTrend policyTrend = new PolicyTrend();
		if (!optionalPolicyTrend.isPresent()) {
			policyTrend.setPolicyId(optionalPolicy.get().getPolicyId());
			policyTrend.setCount(1l);
			policyTrendRepository.save(policyTrend);
		} else {
			policyTrend.setPolicyId(optionalPolicy.get().getPolicyId());
			policyTrend.setCount(optionalPolicyTrend.get().getCount() + 1);
			policyTrendRepository.save(policyTrend);
		}
		LOGGER.info("exiting getAllPolicies method");
		return AppConstants.SUCCESS;
	}
	
	
	

}
