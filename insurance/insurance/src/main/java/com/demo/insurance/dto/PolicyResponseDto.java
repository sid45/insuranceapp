package com.demo.insurance.dto;

public class PolicyResponseDto {
	
	
	private long policyId;
	
	private String policyName;
	
	private int minEntryAge;
	
	private int maxEntryAge;
	
	private int maximumMaturityAge;
	
	private int minPolicyTerm;
	
	private int maxPolicyTerm;
	
	private String minimumPremium;
	
	private String minSumAssured;
	
	
	public long getPolicyId() {
		return policyId;
	}
	public void setPolicyId(long policyId) {
		this.policyId = policyId;
	}
	public String getPolicyName() {
		return policyName;
	}
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}
	public int getMinEntryAge() {
		return minEntryAge;
	}
	public void setMinEntryAge(int minEntryAge) {
		this.minEntryAge = minEntryAge;
	}
	public int getMaxEntryAge() {
		return maxEntryAge;
	}
	public void setMaxEntryAge(int maxEntryAge) {
		this.maxEntryAge = maxEntryAge;
	}
	public int getMaximumMaturityAge() {
		return maximumMaturityAge;
	}
	public void setMaximumMaturityAge(int maximumMaturityAge) {
		this.maximumMaturityAge = maximumMaturityAge;
	}
	public int getMinPolicyTerm() {
		return minPolicyTerm;
	}
	public void setMinPolicyTerm(int minPolicyTerm) {
		this.minPolicyTerm = minPolicyTerm;
	}
	public int getMaxPolicyTerm() {
		return maxPolicyTerm;
	}
	public void setMaxPolicyTerm(int maxPolicyTerm) {
		this.maxPolicyTerm = maxPolicyTerm;
	}
	public String getMinimumPremium() {
		return minimumPremium;
	}
	public void setMinimumPremium(String minimumPremium) {
		this.minimumPremium = minimumPremium;
	}
	public String getMinSumAssured() {
		return minSumAssured;
	}
	public void setMinSumAssured(String minSumAssured) {
		this.minSumAssured = minSumAssured;
	}
	
	
	

}
