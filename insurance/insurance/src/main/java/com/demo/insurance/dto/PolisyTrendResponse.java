package com.demo.insurance.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PolisyTrendResponse {
	
	private String policyName;
	
	private double percentage;

}
