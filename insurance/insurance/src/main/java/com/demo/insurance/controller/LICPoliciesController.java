package com.demo.insurance.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.insurance.dto.PolicyResponseDto;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.service.LICPolicyService;


/**
 * This class is used to list out all policies.
 * @author sidramesh mudhol
 *
 */
@RestController
@RequestMapping("/policies")
public class LICPoliciesController {
	
	
	/**
	 * LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(LICPoliciesController.class);
	
	@Autowired
	LICPolicyService licPolicyService;
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/")
	public ResponseEntity<List<PolicyResponseDto>> getAllPolicies() {
		LOGGER.info("inside getAllPolicies method");
		List<PolicyResponseDto> policyResponseDtoList = licPolicyService.getAllPolicies();
		LOGGER.info("exiting getAllPolicies method");
		return new ResponseEntity<>(policyResponseDtoList, HttpStatus.OK);
		
	}
	
	/**
	 * 
	 * @param policyId
	 * @return PolicyResponseDto
	 * @throws PolicyNotFoundException
	 */
	@GetMapping("/{policyId}")
	public ResponseEntity<PolicyResponseDto> getPolicyById(@PathVariable Long policyId) throws PolicyNotFoundException {
		LOGGER.info("inside getPolicyById method");
		PolicyResponseDto policyResponseDto = licPolicyService.getPolicyById(policyId);
		LOGGER.info("exiting getPolicyById method");
		return new ResponseEntity<>(policyResponseDto, HttpStatus.OK);
		
	}

}
