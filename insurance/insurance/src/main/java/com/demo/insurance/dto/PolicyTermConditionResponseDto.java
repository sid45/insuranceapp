package com.demo.insurance.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PolicyTermConditionResponseDto {
	
	private String termCondition;
	private int statusCode;

}
