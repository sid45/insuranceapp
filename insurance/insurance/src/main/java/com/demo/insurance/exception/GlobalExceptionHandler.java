package com.demo.insurance.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;



@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(PolicyNotFoundException.class)
	public ResponseEntity<ErrorResponse> productsErrorException(PolicyNotFoundException ex) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(601);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}


}
