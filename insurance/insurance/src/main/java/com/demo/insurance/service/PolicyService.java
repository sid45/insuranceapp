package com.demo.insurance.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.insurance.constants.AppConstants;
import com.demo.insurance.dto.PolicyDescResponseDto;
import com.demo.insurance.dto.PolicySalientFeatureDto;
import com.demo.insurance.dto.PolicyTermConditionResponseDto;
import com.demo.insurance.dto.PolicyTrendDto;
import com.demo.insurance.dto.PolisyTrendResponse;
import com.demo.insurance.entity.EmployeePolicy;
import com.demo.insurance.entity.Policy;
import com.demo.insurance.entity.PolicySalientFeature;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.repository.EmpPolicyRepository;
import com.demo.insurance.repository.PolicyRepository;
import com.demo.insurance.repository.PolicySalientFeatureRepository;

@Service
public class PolicyService {
	@Autowired
	PolicyRepository policyRepository;

	@Autowired
	PolicySalientFeatureRepository policySalientFeatureRepository;
	
	@Autowired
	EmpPolicyRepository empPolicyRepository;
	
	public PolicyDescResponseDto policyDesc(long policyId) throws PolicyNotFoundException {
		
		Optional<Policy> policy = policyRepository.findById(policyId);
		if (!policy.isPresent())
			throw new PolicyNotFoundException(AppConstants.POLICY_NOT_FOUND);
		
		PolicyDescResponseDto policyDescResponseDto=new PolicyDescResponseDto();
		BeanUtils.copyProperties(policy, policyDescResponseDto);
		policyDescResponseDto.setMaxEntryAge(policy.get().getMaxEntryAge());
		return policyDescResponseDto;
	}

	public PolicySalientFeatureDto policySalientFeature(long policyId) throws PolicyNotFoundException {
		
		List<PolicySalientFeature> policySalientFeatureList=policySalientFeatureRepository.findByPolicyId(policyId);
		
		if(policySalientFeatureList.isEmpty())
			throw new PolicyNotFoundException(AppConstants.SALIENT_FTR_NOT_FOUND);
		
		List<String> salientFeatureList=new ArrayList<>();
		for (PolicySalientFeature policySalientFeature : policySalientFeatureList) {
			
			salientFeatureList.add(policySalientFeature.getSalientFeature());
			
		}
		PolicySalientFeatureDto policySalientFeatureDto=new PolicySalientFeatureDto();
		policySalientFeatureDto.setSalientFeatureList(salientFeatureList);
		policySalientFeatureDto.setStatusCode(610);
		
		return policySalientFeatureDto;
	}

	public PolicyTermConditionResponseDto policyTermsAdndCondition(long policyId) throws PolicyNotFoundException
	{
		Optional<Policy> policy = policyRepository.findById(policyId);
		if (!policy.isPresent())
			throw new PolicyNotFoundException(AppConstants.POLICY_NOT_FOUND);
		PolicyTermConditionResponseDto policyTermConditionResponseDto=new PolicyTermConditionResponseDto();
		policyTermConditionResponseDto.setTermCondition(policy.get().getTermCondition());
		policyTermConditionResponseDto.setStatusCode(611);
		return policyTermConditionResponseDto;
		
	}

	public List<PolicyTrendDto> policyTrendOn10Record() throws PolicyNotFoundException {
		List<Policy> policies = policyRepository.findAll();
		if (policies.isEmpty())
			throw new PolicyNotFoundException(AppConstants.POLICY_NOT_FOUND);
		List<PolicyTrendDto> policyTrendResponseList = new ArrayList<>();
		List<EmployeePolicy> employeePolicies = empPolicyRepository.findTop10ByOrderByEmpPolicyIdDesc();
		Map<Policy, List<EmployeePolicy>> policyMap = employeePolicies.stream()
				.collect(Collectors.groupingBy(EmployeePolicy::getPolicyId));
		int percentage = 0;
		for (Map.Entry<Policy, List<EmployeePolicy>> entry : policyMap.entrySet()) {
			PolicyTrendDto policyTrendDto = new PolicyTrendDto();
			policyTrendDto.setPolicyId(entry.getKey().getPolicyId());
			policyTrendDto.setCount(entry.getValue().size());
			percentage = (entry.getValue().size() * 100) / 10;
			policyTrendDto.setPercentage(percentage);
			policyTrendResponseList.add(policyTrendDto);

		}
		return policyTrendResponseList;
	}
}
