package com.demo.insurance.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.insurance.dto.PolicyTrendDto;
import com.demo.insurance.service.GlobalTrendPolicyService;

/**
 * This class is used to find out global trend policy.
 * @author sidramesh mudhol
 *
 */
@RestController
@RequestMapping("/policyTrend")
public class GlobalTrendPolicyController {
	
	
	/**
	 * LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalTrendPolicyController.class);
	
	@Autowired
	private GlobalTrendPolicyService globalTrendPolicyService;
	
	/**
	 * This method is for getting global trend.
	 * @return
	 */
	@GetMapping("/")
	public ResponseEntity<List<PolicyTrendDto>> getPolicyTrend() {
		LOGGER.info("inside getPolicyTrend method");
		List<PolicyTrendDto> policyTrendDtoList = globalTrendPolicyService.getPolicyTrend();
		LOGGER.info("exiting getPolicyTrend method");
		return new ResponseEntity<>(policyTrendDtoList, HttpStatus.OK);
		
	}
	

}
