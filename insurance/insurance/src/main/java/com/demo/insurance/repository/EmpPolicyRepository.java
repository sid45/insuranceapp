package com.demo.insurance.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.insurance.entity.EmployeePolicy;
import com.demo.insurance.entity.Policy;


@Repository
public interface EmpPolicyRepository extends JpaRepository<EmployeePolicy, Long> {

	List<EmployeePolicy> findByPolicyId(Policy policy);
   
	List<EmployeePolicy> findFirst10ByPolicyId(Policy policy);

}
