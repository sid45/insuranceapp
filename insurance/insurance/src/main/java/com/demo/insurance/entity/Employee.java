package com.demo.insurance.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table(name="employee")
public class Employee {
	
	@Id
	private long empId;
	
	private String empName;
	
	private int age;
	
	private LocalDate dateOfBirth;
	
	private String phoneNo;
	
	
	
	
	

}
