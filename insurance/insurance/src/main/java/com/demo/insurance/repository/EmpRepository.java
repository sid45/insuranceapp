package com.demo.insurance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.insurance.entity.Employee;

@Repository
public interface EmpRepository extends JpaRepository<Employee, Long> {

}
