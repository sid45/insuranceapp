package com.demo.insurance.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.insurance.dto.PolicyRequestDto;
import com.demo.insurance.exception.EmployeeNotFoundException;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.service.EmpPolicyService;

import io.swagger.annotations.ApiOperation;
/**
 * 
 * This class is used to enroll for a policy.
 * @author sidramesh mudhol
 *
 */
@RestController
@RequestMapping("/policies")
public class EmpPolicyController {
	
	/**
	 * LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpPolicyController.class);
	
	@Autowired
	private EmpPolicyService empPolicyService;
	
	/**
	 * 
	 * @param policyRequestDto
	 * @return
	 * @throws EmployeeNotFoundException
	 * @throws PolicyNotFoundException
	 */
	@PostMapping("/")
	@ApiOperation("policy enrollment")
	public ResponseEntity<String> selectPolicy(@RequestBody PolicyRequestDto policyRequestDto) throws EmployeeNotFoundException, PolicyNotFoundException {
		LOGGER.info("inside selectPolicy method");
		String message = empPolicyService.selectPolicy(policyRequestDto);
		LOGGER.info("exiting selectPolicy method");
		return new ResponseEntity<>(message, HttpStatus.OK);
		
	}
	
	
	

}
