package com.demo.insurance.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PolicySalientFeatureDto {
	
	private List<String> salientFeatureList;
	private int statusCode;

}
