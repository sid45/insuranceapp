package com.demo.insurance.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.insurance.dto.PolicyDescResponseDto;
import com.demo.insurance.dto.PolicySalientFeatureDto;
import com.demo.insurance.dto.PolicyTermConditionResponseDto;
import com.demo.insurance.dto.PolicyTrendDto;
import com.demo.insurance.dto.PolisyTrendResponse;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.service.PolicyService;


@RestController
@RequestMapping("/policies")
public class PolicyController {
	@Autowired
	PolicyService policyService;
	
	
	@GetMapping(value = "/desc/{policyId}/")
	public ResponseEntity<PolicyDescResponseDto> policyDesc(@PathVariable long policyId) throws PolicyNotFoundException
	{
		return new ResponseEntity<>(policyService.policyDesc(policyId), HttpStatus.ACCEPTED);
	}
	
	@GetMapping(value = "/salientFeatures/{policyId}/")
	public ResponseEntity<PolicySalientFeatureDto> policySalientFeature(@PathVariable long policyId) throws PolicyNotFoundException
	{
        return new ResponseEntity<>(policyService.policySalientFeature(policyId), HttpStatus.ACCEPTED);
	}
	
	@GetMapping(value = "/termsAdndCondition/{policyId}/")
	public ResponseEntity<PolicyTermConditionResponseDto> policyTermsAdndCondition(@PathVariable long policyId) throws PolicyNotFoundException
	{
		return new ResponseEntity<>(policyService.policyTermsAdndCondition(policyId), HttpStatus.ACCEPTED);
	}
	
	@GetMapping(value = "/trendOn10Record")
	public ResponseEntity<List<PolicyTrendDto>> policyTrendOn10Record() throws PolicyNotFoundException 
	{
		return new ResponseEntity<>(policyService.policyTrendOn10Record(), HttpStatus.ACCEPTED);
	}

}
