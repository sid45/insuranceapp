package com.demo.insurance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.insurance.entity.PolicyTrend;


@Repository
public interface PolicyTrendRepository extends JpaRepository<PolicyTrend, Long> {

}
