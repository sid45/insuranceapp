package com.demo.insurance.dto;

public class PolicyRequestDto {
	
	private long policyId;
	
	private long empId;

	public long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(long policyId) {
		this.policyId = policyId;
	}

	public long getEmpId() {
		return empId;
	}

	public void setEmpId(long empId) {
		this.empId = empId;
	}
	
	

}
