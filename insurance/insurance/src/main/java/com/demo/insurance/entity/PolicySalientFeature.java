package com.demo.insurance.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table
public class PolicySalientFeature
{
	@Id
	private long policySalientFeatureId;
	private long policyId;
	private String salientFeature;
	
}
