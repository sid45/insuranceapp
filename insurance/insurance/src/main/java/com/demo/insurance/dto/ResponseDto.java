package com.demo.insurance.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseDto {
	
private String msg;
private int statusCode;

}
