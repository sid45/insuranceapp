package com.demo.insurance.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table(name="employee_policy")
public class EmployeePolicy {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long empPolicyId;
	
	@ManyToOne
	@JoinColumn(name="empid")
	private Employee empId;
	
	@ManyToOne
	@JoinColumn(name="policyId")
	private Policy policyId;
	
	private LocalDate date;
	
	private double minimumPremium;
	
	
	
	
	

}
