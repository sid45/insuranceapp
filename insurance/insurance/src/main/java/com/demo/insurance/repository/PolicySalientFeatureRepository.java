package com.demo.insurance.repository;

import java.util.List;


import org.springframework.data.repository.CrudRepository;


import org.springframework.stereotype.Repository;
import com.demo.insurance.entity.PolicySalientFeature;


@Repository
public interface PolicySalientFeatureRepository extends CrudRepository<PolicySalientFeature, Long> {

	List<PolicySalientFeature> findByPolicyId(long policyId);
	
	
}
