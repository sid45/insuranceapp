package com.demo.insurance.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="policytrend")
public class PolicyTrend {
	
	@Id
	private long policyId;
	
	private long count;

}
