package com.demo.insurance.controllerTest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.demo.insurance.controller.PolicyController;
import com.demo.insurance.dto.PolicyDescResponseDto;
import com.demo.insurance.dto.PolicySalientFeatureDto;
import com.demo.insurance.dto.PolicyTermConditionResponseDto;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.service.PolicyService;

@SpringBootTest
class PolicyControllerTest {

	@Mock
	PolicyService policyService;

	@InjectMocks
	PolicyController policyController;

	PolicyDescResponseDto policyDescResponseDto;
	PolicySalientFeatureDto policySalientFeatureDto;
	List<String> salientFeatureList;
	PolicyTermConditionResponseDto policyTermConditionResponseDto;

	@BeforeEach
	public void setup() {
		policyTermConditionResponseDto = new PolicyTermConditionResponseDto();
		policyTermConditionResponseDto.setStatusCode(612);
		policyTermConditionResponseDto.setTermCondition("okey");

		policyTermConditionResponseDto.setTermCondition("omg");
		policyTermConditionResponseDto.setStatusCode(610);
		policyDescResponseDto = new PolicyDescResponseDto();
		policyDescResponseDto.setMaxEntryAge(23);
		policyDescResponseDto.setMaximumMaturityAge(78);
		policyDescResponseDto.setMaxPolicyTerm(89);
		policyDescResponseDto.setMinEntryAge(45);
		policyDescResponseDto.setMinimumPremium("10000");
		policyDescResponseDto.setMinPolicyTerm(34);
		policyDescResponseDto.setMinSumAssured("1000000");
		policyDescResponseDto.setPolicyName("polily");
		salientFeatureList = new ArrayList<>();

		salientFeatureList.add("what");
		salientFeatureList.add("is");
		salientFeatureList.add("this");
		policySalientFeatureDto = new PolicySalientFeatureDto();

		policySalientFeatureDto.setSalientFeatureList(salientFeatureList);

		policySalientFeatureDto.setStatusCode(610);
	}

	@Test
	public void policyDescTest() throws PolicyNotFoundException {

		Mockito.when(policyService.policyDesc(Mockito.anyLong())).thenReturn(policyDescResponseDto);
		ResponseEntity<PolicyDescResponseDto> actualValue = policyController.policyDesc(Mockito.anyLong());
		Assertions.assertEquals(policyDescResponseDto.getMaxEntryAge(), actualValue.getBody().getMaxEntryAge());

	}

	@Test
	public void policySalientFeatureTest() throws PolicyNotFoundException {

		Mockito.when(policyService.policySalientFeature(Mockito.anyLong())).thenReturn(policySalientFeatureDto);
		ResponseEntity<PolicySalientFeatureDto> actualValue = policyController.policySalientFeature(Mockito.anyLong());
		Assertions.assertEquals(policySalientFeatureDto.getStatusCode(), actualValue.getBody().getStatusCode());

	}

	@Test
	public void policyTermsAdndConditionTest() throws PolicyNotFoundException {

		Mockito.when(policyService.policyTermsAdndCondition(Mockito.anyLong()))
				.thenReturn(policyTermConditionResponseDto);
		ResponseEntity<PolicyTermConditionResponseDto> actualValue = policyController
				.policyTermsAdndCondition(Mockito.anyLong());
		Assertions.assertEquals(policyTermConditionResponseDto.getStatusCode(), actualValue.getBody().getStatusCode());

	}

}
