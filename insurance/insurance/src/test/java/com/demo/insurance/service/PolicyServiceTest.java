package com.demo.insurance.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.insurance.dto.PolicyDescResponseDto;
import com.demo.insurance.dto.PolicySalientFeatureDto;
import com.demo.insurance.dto.PolicyTermConditionResponseDto;
import com.demo.insurance.dto.PolisyTrendResponse;
import com.demo.insurance.entity.EmployeePolicy;
import com.demo.insurance.entity.Policy;
import com.demo.insurance.entity.PolicySalientFeature;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.repository.EmpPolicyRepository;
import com.demo.insurance.repository.PolicyRepository;
import com.demo.insurance.repository.PolicySalientFeatureRepository;

@SpringBootTest
class PolicyServiceTest {
	
     @Mock
	PolicyRepository policyRepository;

     @Mock
	PolicySalientFeatureRepository policySalientFeatureRepository;
	
     @Mock
	EmpPolicyRepository empPolicyRepository;
     
     @InjectMocks
     PolicyService policyService;

     Policy policy;
     List<PolicySalientFeature> PolicySalientFeatureList;
     PolicySalientFeature policySalientFeature;
     List<EmployeePolicy> employeePolicyList;
     List<Policy> policies;
     
 	@BeforeEach
 	public void setParameters() {
 		
 		policy = new Policy();
 		policy.setPolicyId(1l);
 		policy.setPolicyName("LICJeevan");
 		policy.setTermCondition("termCondition");
 		policy.setMaxEntryAge(37);
 		
 		PolicySalientFeatureList=new ArrayList<>();
 		policySalientFeature=new PolicySalientFeature();
 		policySalientFeature.setSalientFeature("hello");
 		policySalientFeature.setPolicyId(1);
 		policySalientFeature.setPolicySalientFeatureId(1);
 		
 		PolicySalientFeatureList.add(policySalientFeature);
 		
 		employeePolicyList=new ArrayList<>();
 		EmployeePolicy employeePolicy=new EmployeePolicy();
 		employeePolicyList.add(employeePolicy);
 		
 		policies=new ArrayList<>();
 		policies.add(policy);
 		
 	}
	@Test
	public void policyDesc() throws PolicyNotFoundException {
	
		
		Mockito.when(policyRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(policy));
		
		PolicyDescResponseDto policyDescResponseDto = policyService.policyDesc(Mockito.anyLong());
		Assertions.assertEquals(policy.getMaxEntryAge(), policyDescResponseDto.getMaxEntryAge());
}
	
	@Test
	public void policySalientFeature() throws PolicyNotFoundException {
	
		
		Mockito.when(policySalientFeatureRepository.findByPolicyId(Mockito.anyLong())).thenReturn(PolicySalientFeatureList);
		
		PolicySalientFeatureDto policySalientFeatureDto = policyService.policySalientFeature(Mockito.anyLong());
		Assertions.assertEquals(610, policySalientFeatureDto.getStatusCode());
}
	
	@Test
	public void policyTermsAdndCondition() throws PolicyNotFoundException {
	
		
		Mockito.when(policyRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(policy));
		
		PolicyTermConditionResponseDto policyTermConditionResponseDto = policyService.policyTermsAdndCondition(Mockito.anyLong());
		Assertions.assertEquals(611, policyTermConditionResponseDto.getStatusCode());
}
	
	@Test
	public void policyTrendOn10Record() throws PolicyNotFoundException {
	
		
		Mockito.when(policyRepository.findAll()).thenReturn(policies);
		Mockito.when(empPolicyRepository.findFirst10ByPolicyId(Mockito.any(Policy.class))).thenReturn(employeePolicyList);
		
		List<PolisyTrendResponse> polisyTrendResponse = policyService.policyTrendOn10Record();
		Assertions.assertEquals(1, polisyTrendResponse.size());
}
}
