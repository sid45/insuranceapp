package com.demo.insurance.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.demo.insurance.dto.PolicyTrendDto;
import com.demo.insurance.service.GlobalTrendPolicyService;

@SpringBootTest
class GlobalTrendPolicyControllerTest {
	
	@Mock
	GlobalTrendPolicyService globalTrendPolicyService;
	
	@InjectMocks
	GlobalTrendPolicyController controller;

	@Test
	public void testGetPolicyTrend() {
		List<PolicyTrendDto> policyTrendDtoList = new ArrayList<PolicyTrendDto>();
		Mockito.when(globalTrendPolicyService.getPolicyTrend()).thenReturn(policyTrendDtoList);
		ResponseEntity<List<PolicyTrendDto>> response = controller.getPolicyTrend();
		
		
		
	}

}
