package com.demo.insurance.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import com.demo.insurance.dto.PolicyResponseDto;
import com.demo.insurance.entity.Policy;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.repository.PolicyRepository;


@SpringBootTest
class LICPolicyServiceTest {
	
	@Mock
	PolicyRepository policyRepository;
	
	@InjectMocks
	LICPolicyService licPolicyService;
	
	
	Policy policy = new Policy();
	
	@BeforeEach
	public void setParameters() {
		policy.setPolicyId(1l);
		policy.setPolicyName("LICJeevan");
	}

	@Test
	public void testGetAllPolicies() {
		
		List<Policy> policyList = new ArrayList<Policy>();
		policyList.add(policy);
		Mockito.when(policyRepository.findAll()).thenReturn(policyList );
		List<PolicyResponseDto> policyResponseDtoList = licPolicyService.getAllPolicies();
		Assertions.assertEquals("LICJeevan", policyResponseDtoList.get(0).getPolicyName());
		
		
	}
	
	
	@Test
	public void testGetPolicyById() throws PolicyNotFoundException {
		Optional<Policy> optionalPolicy = Optional.of(policy);
		Mockito.when(policyRepository.findById(1l)).thenReturn(optionalPolicy );
		PolicyResponseDto policyResponseDto = licPolicyService.getPolicyById(1l);
		Assertions.assertEquals("LICJeevan", policyResponseDto.getPolicyName());

		
	}
	
	
	
	
	
	
	
	

}
