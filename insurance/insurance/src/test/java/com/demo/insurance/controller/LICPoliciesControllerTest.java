package com.demo.insurance.controller;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.demo.insurance.dto.PolicyResponseDto;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.service.LICPolicyService;

@SpringBootTest
class LICPoliciesControllerTest {
	
	@Mock
	LICPolicyService licPolicyService;
	
	@InjectMocks
	LICPoliciesController policyController;

	@Test
	public void testGetAllPolicies() {
		List<PolicyResponseDto> policyResponseDtoList = new ArrayList<PolicyResponseDto>();
		PolicyResponseDto dto = new PolicyResponseDto();
		policyResponseDtoList.add(dto);
		Mockito.when(licPolicyService.getAllPolicies()).thenReturn(policyResponseDtoList);
		ResponseEntity<List<PolicyResponseDto>> response = policyController.getAllPolicies();
		
		
	}
	
	@Test
	public void testGetPolicyById() throws PolicyNotFoundException {
		
		PolicyResponseDto policyResponseDto = new PolicyResponseDto();
		policyResponseDto.setPolicyName("LICeevan");
		Mockito.when(licPolicyService.getPolicyById(Mockito.anyLong())).thenReturn(policyResponseDto );
		ResponseEntity<PolicyResponseDto> response = policyController.getPolicyById(1l);
		Assertions.assertEquals("LICeevan", response.getBody().getPolicyName());

		
		
	}

}
