package com.demo.insurance.service;

import static org.junit.jupiter.api.Assertions.fail;


import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.insurance.constants.AppConstants;
import com.demo.insurance.dto.PolicyRequestDto;
import com.demo.insurance.entity.Employee;
import com.demo.insurance.entity.Policy;
import com.demo.insurance.entity.PolicyTrend;
import com.demo.insurance.exception.EmployeeNotFoundException;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.repository.EmpPolicyRepository;
import com.demo.insurance.repository.EmpRepository;
import com.demo.insurance.repository.PolicyRepository;
import com.demo.insurance.repository.PolicyTrendRepository;


@SpringBootTest
class EmpPolicyServiceTest {
	
	@Mock
	EmpRepository empRepository;
	
	@Mock
	PolicyRepository policyRepository;
	
	@Mock
	EmpPolicyRepository empPolicyRepository;
	
	@Mock
	PolicyTrendRepository policyTrendRepository;
	
	@InjectMocks
	EmpPolicyService empPolicyService;
	
	Employee employee = new Employee();
	Policy policy = new Policy();
	
	@BeforeEach
	public void setParameters() {
		employee.setEmpId(1l);
		employee.setAge(21);
		employee.setEmpName("sid");
		policy.setPolicyId(1l);
		policy.setPolicyName("LICJeevan");
		policy.setTermCondition("termCondition");
	}
	
	

	@Test
	public void testSelectPolicy() throws EmployeeNotFoundException, PolicyNotFoundException {
		PolicyRequestDto policyRequestDto = new PolicyRequestDto();
		Optional<Employee> employeeOptional = Optional.of(employee);
		Mockito.when(empRepository.findById(Mockito.anyLong())).thenReturn(employeeOptional);
		Optional<Policy> optionalPolicy = Optional.of(policy);
		Mockito.when(policyRepository.findById(Mockito.anyLong())).thenReturn(optionalPolicy );
		PolicyTrend policyTrend = new PolicyTrend();
		Optional<PolicyTrend> optionalPolicyTrend = Optional.of(policyTrend);
		Mockito.when(policyTrendRepository.findById(Mockito.anyLong())).thenReturn(optionalPolicyTrend );
		String message = empPolicyService.selectPolicy(policyRequestDto);
		Assertions.assertEquals(AppConstants.SUCCESS, message);
		
	}
	
	@Test
	public void testSelectPolicyOne() throws EmployeeNotFoundException, PolicyNotFoundException {
		PolicyRequestDto policyRequestDto = new PolicyRequestDto();
		Optional<Employee> employeeOptional = Optional.of(employee);
		Mockito.when(empRepository.findById(Mockito.anyLong())).thenReturn(employeeOptional);
		Optional<Policy> optionalPolicy = Optional.of(policy);
		Mockito.when(policyRepository.findById(Mockito.anyLong())).thenReturn(optionalPolicy );
		
		Optional<PolicyTrend> optionalPolicyTrend = Optional.empty();
		Mockito.when(policyTrendRepository.findById(Mockito.anyLong())).thenReturn(optionalPolicyTrend );
		String message = empPolicyService.selectPolicy(policyRequestDto);
		Assertions.assertEquals(AppConstants.SUCCESS, message);
		
	}

}
