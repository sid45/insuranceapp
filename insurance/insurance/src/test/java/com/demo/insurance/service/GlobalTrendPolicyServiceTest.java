package com.demo.insurance.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.insurance.entity.PolicyTrend;
import com.demo.insurance.repository.PolicyTrendRepository;


@SpringBootTest
class GlobalTrendPolicyServiceTest {
	
	
	@Mock
	PolicyTrendRepository policyTrendRepository;
	
	
	@InjectMocks
	GlobalTrendPolicyService globalTrendPolicyService;
	

	@Test()
	public void testGetPolicyTrend() {
		
		List<PolicyTrend> policytrendList = new ArrayList<PolicyTrend>();
		PolicyTrend policytrend = new PolicyTrend();
		policytrend.setCount(10);
		policytrend.setPolicyId(1l);
		policytrendList.add(policytrend);
		Mockito.when(policyTrendRepository.findAll()).thenReturn(policytrendList);
		globalTrendPolicyService.getPolicyTrend();
		
	}

}
