package com.demo.insurance.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.demo.insurance.constants.AppConstants;
import com.demo.insurance.dto.PolicyRequestDto;
import com.demo.insurance.exception.EmployeeNotFoundException;
import com.demo.insurance.exception.PolicyNotFoundException;
import com.demo.insurance.service.EmpPolicyService;


@SpringBootTest
class EmpPolicyControllerTest {
	
	
	@Mock
	EmpPolicyService empPolicyService;
	
	@InjectMocks
	EmpPolicyController empPolicyController;
	
	@Test
	public void testSelectPolicy() throws EmployeeNotFoundException, PolicyNotFoundException {
		PolicyRequestDto policyRequestDto = new PolicyRequestDto();
		Mockito.when(empPolicyService.selectPolicy(policyRequestDto)).thenReturn("success");
		ResponseEntity<String> message = empPolicyController.selectPolicy(policyRequestDto);
		Assertions.assertEquals("success", message.getBody());

		
	}

}
